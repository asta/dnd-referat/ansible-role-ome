#!/bin/bash

# siehe: https://airensoft.gitbook.io/ovenmediaengine/access-control/signedpolicy

[ ! -f "signed_policy_url_generator.sh" ] && curl "https://raw.githubusercontent.com/AirenSoft/OvenMediaEngine/master/misc/signed_policy_url_generator.sh"  > signed_policy_url_generator.sh && chmod +x "signed_policy_url_generator.sh"

#url_expire
#url_activate
#stream_expire
#allow_ip

#read -r -p ":q: " URLEXPIRESEC
#read -r -p "Seconds until url expires: " URLEXPIRESEC
#read -r -p "Seconds until stream expires[${URLEXPIRESEC}]: " STREAMEXPIRESEC
#read -r -p "Seconds until url activate[0]: " URLACTIVATESEC
#
#

if [ "$#" -lt "1" ]; then
	read -r -s -p "Secret: " SECRET
	echo ""
else
	SECRET="$1"
fi
#echo "Secret: \"${SECRET}\""

if [ "$#" -lt "2" ]; then
	URLEXPIRESEC="600"
else
	URLEXPIRESEC="$2"
fi
STREAMEXPIRESEC=""
URLACTIVATESEC=""

if [ "x${STREAMEXPIRESEC}" = "x" ]; then
	STREAMEXPIRESEC="${URLEXPIRESEC}"
fi

if [ "x${URLACTIVATESEC}" = "x" ]; then
	URLACTIVATESEC="0"
fi


# stream can start 10 minutes earlier
URLACTIVATEJIGGLESEC=6000

CURTIME="$(date +"%s000")"

URLEXPIRE="$(( $CURTIME + ( $URLEXPIRESEC * 1000 ) ))"
STREAMEXPIRE="$(( $CURTIME + ( $STREAMEXPIRESEC * 1000 ) ))"
URLACTIVATE="$(( $CURTIME + ( ( $URLACTIVATESEC - $URLACTIVATEJIGGLESEC ) * 1000 ) ))"


POLICY="$(printf '{"url_expire":%s,"url_activate":%s,"stream_expire":%s}' "${URLEXPIRE}" "${URLACTIVATE}" "${STREAMEXPIRE}")"

echo "Policy:"
echo "${POLICY}" | jq '.'


DOMAIN="test01.asta.uni-goettingen.de"
WEBRTCPORT="3334"
WEBRTCPROTOCOL="wss"
APPNAME="app"
STREAMNAME="stream"
SIGNATUREQUERYKEYNAME="signature"
POLICYQUERYKEYNAME="policy"

#echo ./signed_policy_url_generator.sh "${SECRET}" "${WEBRTCPROTOCOL}://${DOMAIN}:${WEBRTCPORT}/${APPNAME}/${STREAMNAME}" "${SIGNATUREQUERYKEYNAME}" "${POLICYQUERYKEYNAME}" "${POLICY}"
RESRTMP="$(./signed_policy_url_generator.sh "${SECRET}" "rtmp://${DOMAIN}:1935/${APPNAME}/${STREAMNAME}" "${SIGNATUREQUERYKEYNAME}" "${POLICYQUERYKEYNAME}" "${POLICY}")"
#RESWEBRTC="$(./signed_policy_url_generator.sh "${SECRET}" "${WEBRTCPROTOCOL}://${DOMAIN}:${WEBRTCPORT}/${APPNAME}/${STREAMNAME}" "${SIGNATUREQUERYKEYNAME}" "${POLICYQUERYKEYNAME}" "${POLICY}")"
#RESHLS="$(./signed_policy_url_generator.sh "${SECRET}" "https://${DOMAIN}:443/${APPNAME}/${STREAMNAME}" "${SIGNATUREQUERYKEYNAME}" "${POLICYQUERYKEYNAME}" "${POLICY}")"


echo "RESWEBRTC: ${RESWEBRTC}"
#echo "RESHLS: ${RESHLS}"
#echo "RESRTMP: ${RESRTMP}"

echo ""
echo ""
echo ""

#echo "${RESRTMP}" | grep '^\[URL\]' | cut -d ' ' -f 2
RTMPOBSSERVER="$(echo -n "${RESRTMP}" | grep '^\[URL\]' | cut -d ' ' -f 2 | tr '/' '\n' | head -n -1 | tr '\n' '/' | sed 's,/$,,')"
RTMPOBSKEY="$(echo -n "${RESRTMP}" | grep '^\[URL\]' | cut -d ' ' -f 2 | tr '/' '\n' | tail -n -1 | tr '\n' '/' | sed 's,/$,,')"
#echo -n "${RESRTMP}" | grep '^\[URL\]' | cut -d ' ' -f 2 | tr '/' '\n' | tail -n -1

echo "==>> OBS Settings <<=="
echo "Server: ${RTMPOBSSERVER}"
echo "Key: ${RTMPOBSKEY}"

echo ""
echo ""
echo ""
echo "==> Player Settings <<=="
echo "WEBRTC: ${WEBRTCPROTOCOL}://${DOMAIN}:${WEBRTCPORT}/${APPNAME}/${STREAMNAME}"
echo "HLS: https://${DOMAIN}/${APPNAME}/${STREAMNAME}/playlist.m3u8"

